package piwords.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        // Input is a 4 digit number, 0.123 represented in base 4
        int[] input = {0, 1, 2, 3};

        // Want to map 0 -> "d", 1 -> "c", 2 -> "b", 3 -> "a"
        char[] alphabet = {'d', 'c', 'b', 'a'};

        String expectedOutput = "dcba";
        assertEquals(expectedOutput,
                     DigitsToStringConverter.convertDigitsToString(
                             input, 4, alphabet));
        
        // Input is a 6 digit number, 5.45321 represented in base 6
        int[] input1 = {5, 4, 5, 3, 2, 1};
        char[] alphabet1 = {'a', 'b', 'c', 'd', 'e', 'f'};
        String expectedOutput1 = "fefdcb";
        
        assertEquals(expectedOutput1,
                DigitsToStringConverter.convertDigitsToString(
                        input1, 6, alphabet1));
        
        
    
        // Returns null because base < digit[i]
	    assertEquals(null,
	                 DigitsToStringConverter.convertDigitsToString(
	                         input, 2, alphabet));
	    
	    // Returns null because base != alphabet.length
	    assertEquals(null,
                DigitsToStringConverter.convertDigitsToString(
                        input, 6, alphabet));
	    
	    
    }
}
