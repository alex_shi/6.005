package piwords.src;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
    @Test
    public void basicPowerModTest() {
        // 5^7 mod 23 = 17
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
    	// Additional tests 
    	assertEquals(12, PiGenerator.powerMod(3, 7, 15));
    	assertEquals(-1, PiGenerator.powerMod(-1, 0, 1));
    	assertEquals(-1, PiGenerator.powerMod(-1, -5, -10));
    	assertEquals(1, PiGenerator.powerMod(100, 0, 10));
    }
    @Test
    public void basicComputePiInHexTest(){
    	// Test for computePiInHex
    	//Pi in Hex: 243F6A8885A308D (first 14 digits)
    	//which is 2, 4, 3, 15, 6, 10, 8, 8, 8, 5, 10, 3, 0, 8, 13
    	int[] array1 = {2, 4, 3, 15};
    	int[] array2 = {2, 4, 3, 15, 6, 10, 8, 8, 8, 5, 10, 3, 0, 8, 13};
    	assertArrayEquals(array1, PiGenerator.computePiInHex(4)); 
    	assertArrayEquals(array2, PiGenerator.computePiInHex(15)); 
    	
    }
}
