package piwords.src;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFinderTest {
    @Test
    public void basicGetSubstringsTest() {
        String haystack = "abcde";
        String[] needles = {"ab", "abc", "de", "fg"};

        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("ab", 0);
        expectedOutput.put("abc", 0);
        expectedOutput.put("de", 3);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              needles));
        //test case for no needle matches
        String haystack1 = "qwertyuiop";
        String[] needles1 = {"ab", "abc", "de", "fg"};

        Map<String, Integer> expectedOutput1 = new HashMap<String, Integer>();

        assertEquals(expectedOutput1, WordFinder.getSubstrings(haystack1,
                                                              needles1));
        
        String haystack2 = "qwertyuiop";
        String[] needles2 = {"qw", "123", "iop", "fg", "p"};

        Map<String, Integer> expectedOutput2 = new HashMap<String, Integer>();
        expectedOutput2.put("qw", 0);
        expectedOutput2.put("iop", 7);
        expectedOutput2.put("p", 9);
        expectedOutput2.put("p", 9);
        expectedOutput2.put("p", 9);
        expectedOutput2.put("p", 9);

        assertEquals(expectedOutput2, WordFinder.getSubstrings(haystack2,                                                             needles2));
    }
}
