/**
 * Author: dnj
 * 6.005 Elements of Software Construction
 * (c) 2008-9, MIT and Daniel Jackson
 */
package piano;

import java.applet.Applet;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import music.Pitch;

/**
 * An applet which listens for key presses and calls a set of corresponding functions in PianoMachine.
 * 
 * You can run this class from Eclipse using Run -> Run As -> Java Applet
 */
public class PianoApplet extends Applet {

    private static final long serialVersionUID = -580070854133088915L;

    public void init() {
    	setBackground(Color.green);
    	final PianoMachine piano = new PianoMachine();

        // this is a standard pattern for associating method calls with GUI
        // events
        // the call to the constructor of KeyAdapter creates an object of an
        // anonymous subclass of KeyAdapter, whose keyPressed method is called
        // when a key is pressed in the GUI
    	addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
            	System.out.println("key pressed");
                char key = (char) e.getKeyCode();
                switch (key) {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    piano.beginNote(new Pitch(key - '1'));
                    System.out.println("key " + key + " pressed.");
                    return;
                case '0':
                    piano.beginNote(new Pitch(9));
                    System.out.println("key " + key + " pressed.");
                    return;
                case '-':
                    piano.beginNote(new Pitch(10));
                    System.out.println("key " + key + " pressed.");
                    return;
                case '=':
                    piano.beginNote(new Pitch(11));
                    System.out.println("key " + key + " pressed.");
                    return;
                case 'r':
                case 'R':
                	boolean isRecording = piano.toggleRecording();
                	if(isRecording) {
                		setBackground(Color.red);
                		System.out.println("Recording ON.");
                	} else {
                		setBackground(Color.green);
                		System.out.println("Recording OFF.");
                	}
                    return;
                case 'p':
                case 'P':
                    piano.playback();
                    System.out.println("MIDI Playback.");
                    return;
                case 'i':
                case 'I':
                	piano.changeInstrument();
                	System.out.println("Instrument Changed.");
                    return;
                case 'c':
                case 'C':
                	piano.shiftDown();
                	System.out.println("Octave shifted DOWN.");
                	return;
                case 'v':
                case 'V':
                	piano.shiftUp();
                	System.out.println("Octave shifted UP.");
                	return;
                	
                case 'z':
                	System.out.println(piano.history());
                	return;
                }
            }
        });

        addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
            	System.out.println("key released");
                char key = (char) e.getKeyCode();
                switch (key) {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    piano.endNote(new Pitch(key - '1'));
                    return;
                case '0':
                    piano.endNote(new Pitch(9));
                    return;
                case '-':
                    piano.endNote(new Pitch(10));
                    return;
                case '=':
                    piano.endNote(new Pitch(11));
                    return;
                }
            }
        });
        
    }

}