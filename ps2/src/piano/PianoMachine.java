package piano;

import java.util.Stack;

import javax.sound.midi.MidiUnavailableException;

import midi.Instrument;
import midi.Midi;
import music.NoteEvent;
import music.Pitch;

public class PianoMachine {
	
	public static int semitones = 0;
	
	private Midi midi;
    
	/**
	 * constructor for PianoMachine.
	 * 
	 * initialize midi device and any other state that we're storing.
	 */
    public PianoMachine() {
    	try {
            midi = Midi.getInstance();
        } catch (MidiUnavailableException e1) {
            System.err.println("Could not initialize midi device");
            e1.printStackTrace();
            return;
        }
    }
    /**
     * 
     * @param rawPitch
     */
    public void beginNote(Pitch rawPitch) {
    	midi.beginNote(rawPitch.toMidiFrequency() + semitones);
    }

    //    	midi.endNote(rawPitch.toMidiFrequency()); <-- orginial in case I fuck up
    public void endNote(Pitch rawPitch) {
    	midi.endNote(rawPitch.toMidiFrequency() + semitones);
    }

    public void changeInstrument() {
    	midi.DEFAULT_INSTRUMENT = midi.DEFAULT_INSTRUMENT.next();
    }

    public void shiftUp() {
    	semitones = semitones + Pitch.OCTAVE;
    }

    public void shiftDown() {
    	semitones = semitones - Pitch.OCTAVE;
    }
    
    public static boolean isRecording = false;
    public boolean toggleRecording() {
    	if (isRecording == true){
    		isRecording = false;
    	}
    	else{
    		isRecording = true;
    	}
    	return isRecording;
    }
    public static Stack<music.NoteEvent> recordedLog = new Stack<music.NoteEvent>();
    public void captureRecording(){
    	if (isRecording == true){
    		recordedLog.push(music.NoteEvent(instr)
    	}
    }
    
    //TODO write method spec
    protected void playback() {    	
        //TODO: implement for question 4
    }

}
