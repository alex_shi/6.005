package piano;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.sound.midi.MidiUnavailableException;

import midi.Midi;
import music.Pitch;

import org.junit.Test;

public class PianoMachineTest {
	
	PianoMachine pm = new PianoMachine();
	
    @Test
    public void singleNoteTest() throws MidiUnavailableException {
        String expected0 = "on(61,PIANO) wait(100) off(61,PIANO)";
        
    	Midi midi = Midi.getInstance();
    	midi.clearHistory();
    	
        pm.beginNote(new Pitch(1));
		Midi.wait(100);
		pm.endNote(new Pitch(1));

        System.out.println(midi.history());
        assertEquals(expected0,midi.history());
    }

    @Test
    public void changeInstrumentTest() throws MidiUnavailableException {
    	String expected1 = "on(65,BRIGHT_PIANO) wait(10) off(65,BRIGHT_PIANO)";

    	Midi midi = Midi.getInstance();
    	midi.clearHistory();

    	pm.changeInstrument();
    	pm.beginNote(new Pitch(5));
		Midi.wait(10);
		pm.endNote(new Pitch(5));

        System.out.println(midi.history());
        assertEquals(expected1,midi.history());   
        // make error cases
    }
    
    @Test
    public void changePitchTest() throws MidiUnavailableException {
    	String expected2 = "on(65,BRIGHT_PIANO) wait(10) off(77,BRIGHT_PIANO)";
    	
    	Midi midi = Midi.getInstance();
    	midi.clearHistory();
    	
    	pm.beginNote(new Pitch(5));
    	pm.shiftUp();
    	Midi.wait(10);
    	pm.endNote(new Pitch(5));
    	
    	System.out.println(midi.history());
        assertEquals(expected2,midi.history());   
    }
    
}
