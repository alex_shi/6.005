package calculator;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Matcher;

import org.junit.Test;

import calculator.Lexer.Token;

public class CalculatorTest {

	/* Test: Lexer.java 
	 * Takes constructor 'lexer.java' with a String input and creates an arraylist 
	 * of tokens to be used by a parser method.
	 * For example, "3pt / 6 pt" should return a 
	 * stack ordered: Token(NUMBER, "3"), Token(UNIT, "pt"), Token(OPERATOR, "/"),
	 * Token(NUMBER, "6"), Token(UNIT, "pt")
	 * 
	 * Order does not matter in the lexer, so long as the appropriate symbols are used.
	*/
	
	@Test
	public void lexerTest1() {
		ArrayList<Token> testList = new ArrayList<Token>();
		testList.add(new Token(Type.NUMBER, "3"));
		
		Lexer createdLexer = new Lexer("3");
		ArrayList<Token> createdList = createdLexer.tokenList;
		
		for(int i = 0; i < createdList.size(); i++){
			assertTrue(Lexer.equals(createdList.get(i), testList.get(i)));
		}
	}
	
	@Test
	public void lexerTest2() {
		ArrayList<Token> testList = new ArrayList<Token>();
		testList.add(new Token(Type.OPERATOR, "+"));
		
		Lexer createdLexer = new Lexer("-");
		ArrayList<Token> createdList = createdLexer.tokenList;
		
		for(int i = 0; i < createdList.size(); i++){
			assertTrue(Lexer.equals(createdList.get(i), testList.get(i)));
		}
	}
	
	@Test
	public void testGetMatch() {
		Lexer lexer = new Lexer("6in + 7");
		Matcher numberMat = Lexer.NUMBERMATCH.matcher("6in + 7");
		assertEquals("6", lexer.getMatch(numberMat));
		// We should be able to call it twice because the matcher should be reset.
		assertEquals("6", lexer.getMatch(numberMat));
		
		try {
			Matcher failMat = Lexer.NUMBERMATCH.matcher("abc");
			lexer.getMatch(failMat);
			fail("Expected IllegalStateException");
		} catch (IllegalStateException e) {
			// Do nothing, this is expected behavior.
		}		
	}

	@Test
	public void equalsTest() {
		Token a = new Token(Type.OPERATOR, "+");
		Token b = new Token(Type.OPERATOR, "+");
		
		assertTrue(Lexer.equals(a,  b));
	}

	boolean approxEquals(String expr1, String expr2, boolean compareUnits) {
		return new Value(expr1).approxEquals(new Value(expr2), compareUnits);
	}

	static class Value {
		static float delta = 0.001f;
 
		enum Unit {
			POINT, INCH, SCALAR
		}

		Unit unit;
		// in points if a length
		float value;

		Value(String value) {
			value = value.trim();
			if (value.endsWith("pt")) {
				unit = Unit.POINT;
				this.value = Float.parseFloat(value.substring(0,
						value.length() - 2).trim());
			} else if (value.endsWith("in")) {
				unit = Unit.INCH;
				this.value = 72 * Float.parseFloat(value.substring(0,
						value.length() - 2).trim());
			} else {
				unit = Unit.SCALAR;
				this.value = Float.parseFloat(value);
			}
		}

		boolean approxEquals(Value that, boolean compareUnits) {
			return (this.unit == that.unit || !compareUnits)
					&& Math.abs(this.value - that.value) < delta;
		}
	}

}
