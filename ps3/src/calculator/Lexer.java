package calculator;

import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import calculator.Type;

/**
 * Calculator lexical analyzer.
 */
public class Lexer {
	// All these regexes ONLY search the beginning of the line (\^)
	static final Pattern NUMBERMATCH = Pattern.compile("^\\d++");
	static final Pattern OPERATORMATCH = Pattern.compile("[+]");
	static final Pattern UNITMATCH = Pattern.compile("[(in)|(pt)]");

	/**
	 * Token in the stream.
	 */
	public static class Token {
		final Type type;
		final String text;

		Token(Type type, String text) {
			this.type = type;
			this.text = text;
		}

		Token(Type type) {
			this(type, null);
		}
		
		public boolean equals(Object obj) {
			Token other = (Token) obj;
			return (this.type.equals(other.type)
					&& this.text.equalsIgnoreCase(other.text));
		}
	}
	
	/**
	 * Takes two token arguments and returns true if the type 
	 * and text are equal string values. 
	 * Used for JUnit tests.
	 * 
	 * @param a: First token used in equality comparison.
	 * @param other: Second token used in comparison.
	 * @return: Boolean referring to equality of expression
	 
	public static boolean equals(Token a, Token other){
		boolean equality = false;
		if (a.type.equals(other.type) && a.text.equalsIgnoreCase(other.text)){
			equality = true;
		}
		return equality;
	}
	*/

	

	
	@SuppressWarnings("serial")
	static class TokenMismatchException extends Exception {
	}

	//public Stack<Token> tokenList = new Stack<Token>();
	private ArrayList<Token> tokenList = new ArrayList<Token>();
	
	/**
	 * Breaks an input string into a series of tokens using the 
	 * grammar indicated in Parser.java.  For example, the lexer should
	 * identify the inputted string, "3 in * 12 in" as an expression, 
	 * "NUMBER UNIT OPERATOR NUMBER UNIT" and send this information to the 
	 * parser to be evaluated.
	 * 
	 * @param input: Some string input that is to be turned into tokens.
	 */
	public Lexer(String input) {
		
		String rawInput = input.replaceAll("\\s", "");
		boolean validInputs = true;
		while(validInputs) {

			Matcher numberMat = NUMBERMATCH.matcher(rawInput);
			
			if (numberMat.find()) {
				int end = numberMat.end();
				tokenList.add(new Token(Type.NUMBER, rawInput.substring(0, end)));
				rawInput = rawInput.substring(end);
			}
			
			
			// Non-number cases, for units.
			else if (rawInput.substring(0) == "[a-zA-Z]"){
				Matcher unitMat = UNITMATCH.matcher(rawInput);
				
				if (unitMat.find()) {
					int end = unitMat.end();
					tokenList.add(new Token(Type.UNIT, rawInput.substring(0, end)));
					rawInput = rawInput.substring(end);
				}
			}

			// For non-word characters (operators)
			else if (rawInput.substring(0) == "\\W") {
				Matcher opMat = OPERATORMATCH.matcher(rawInput);
				
				if (opMat.find()) {
					int end = opMat.end();
					tokenList.add(new Token(Type.OPERATOR, rawInput.substring(0, end)));
					rawInput = rawInput.substring(end);
				}
			}
			
			else {
				validInputs = false;
			}
		}
	}
	
	String getMatch(Matcher match) {
		match.reset();
		match.find();
		return match.group();
	}
}
