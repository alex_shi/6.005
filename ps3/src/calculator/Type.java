package calculator;

/*
 * Symbols/Groups
 * NUMBER (0-9), OPERATOR(+,-,/,*,(), etc.), UNIT(scalar, in, pt)
 */

/**
 * Token type.
 */
enum Type {
	NUMBER, OPERATOR, UNIT
}
